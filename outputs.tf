output "result" {
  value = <<EOT

  ############
  # required DNS-records
  #   ${var.faasd_domain}. A ${hcloud_server.faasd.ipv4_address}
  #
  # public url
  #   https://${var.faasd_domain}
  #
  # initial password (admin)
  #   ${random_string.gw_password.result}
  #
  # SSH access
  #   client key: ./gen/id_ecdsa
  #   host fingerprint: ./gen/known_hosts
  #   wrapper script: ./bin/ssh
  ############
  EOT
}
