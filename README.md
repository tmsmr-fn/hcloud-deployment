# hcloud-faasd

*Terraform/cloud-init one-shot deployment of [faasd](https://github.com/openfaas/faasd) on [Hetzner Cloud](https://www.hetzner.com)*

## Quickstart
- `cp config.auto.tfvars.example config.auto.tfvars`
- Adjust `config.auto.tfvars`
- `terraform init && terraform apply`
